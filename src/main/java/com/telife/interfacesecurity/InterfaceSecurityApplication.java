package com.telife.interfacesecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InterfaceSecurityApplication {

    public static void main(String[] args)
    {
        SpringApplication.run(InterfaceSecurityApplication.class, args);
    }

}


