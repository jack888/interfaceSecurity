package com.telife.interfacesecurity.configuration;

import com.telife.interfacesecurity.interceptor.ReqIntercepter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author york
 * @since 2020/3/21
 */
@Configuration
public class ReqWebMvcConfigurer implements WebMvcConfigurer {

    @Autowired
    private ReqIntercepter reqIntercepter;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(reqIntercepter)
        .addPathPatterns("/**")
        .excludePathPatterns("/interface/warning");
    }
}
