package com.telife.interfacesecurity.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author york
 * @since 2020/3/21
 */
@Component
public class RedisUtil {

    @Autowired
    private StringRedisTemplate redis;

    /**
     * 往redis存入字符串
     * @param key 键名
     * @param value 值
     */
    public void setStrKey(String key,String value){
        redis.opsForValue().set(key,value);
    }

    /**
     * 往redis存入字符串，自定义时长
     * @param key 键名
     * @param value 值
     * @param duration 时长
     * @param timeUnit 时间单位
     */
    public void setStrKey(String key, String value, long duration, TimeUnit timeUnit){
        redis.opsForValue().set(key,value,duration,timeUnit);
    }

    /**
     * 修改字符串Key的值而不修改过期时间
     * @param key 键名
     * @param value 值
     */
    public void updateStrValue(String key,String value){
       redis.opsForValue().set(key,value,0);
    }

    /**
     * 根据键名获取字符串值
     * @param key 键名
     * @return
     */
    public String getStrKey(String key){
        return redis.opsForValue().get(key);
    }

    /**
     * 存放List元素
     * @param key 键名
     * @param value 键值
     */
    public void setListKey(String key, String value){
        redis.opsForList().leftPush(key,value);
    }

    /**
     * 存放List集合
     * @param key 键名
     * @param list list集合
     */
    public void setListKey(String key, List list){
        redis.opsForList().leftPushAll(key,list);
    }
    /**
     * 根据键名获取List集合
     * @param key 键名
     */
    public List<String> getListKey(String key){
        return redis.opsForList().range(key,0,-1);
    }

    /**
     * 删除list里的元素
     * @param key List键名
     * @param value 元素的值
     */
    public long removeListElement(String key,String value){
        return redis.opsForList().remove(key,0,value);
    }


    /**
     * 删除Key
     * @param key 键名
     * @return
     */
    public boolean deleteKey(String key){
        return redis.delete(key);
    }

}
