package com.telife.interfacesecurity.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author york
 * @since 2020/3/21
 */
@RestController
@RequestMapping("/interface")
public class MainController {

    @GetMapping("/pathOne")
    public String testPathOne(){
        return "正常访问接口路径--1";
    }

    @GetMapping("/pathTwo")
    public String testPathTwo(){
        return "正常访问接口路径--2";
    }

    @GetMapping("/warning")
    public String reqWarning(){
        return "您的请求过于频繁，请稍后再访问";
    }

    @GetMapping("/error")
    public String reqError(){
        return "未知错误";
    }

}
